package org.openjfx.SRV.FileUploadService;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface FileUploadService {
    public byte[] uploadToServer(File file);

    public String convertToFile(byte[] book);
}
